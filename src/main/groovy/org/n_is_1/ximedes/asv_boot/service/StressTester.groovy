package org.n_is_1.ximedes.asv_boot.service

import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer


/**
 * Service for stress testing
 */
class StressTester {

    int merchantAccountId

    MainService service

    public StressTester(MainService mainService) {
        this.service = mainService
    }

    def runTest() {
        merchantAccountId = createAccount(0)
        def threads = []
        (1..100).each {
            threads.push(new Thread(new TestThread()))
        }

        threads.each { Thread thread ->
            thread.start()
        }
    }

    int createAccount(int overdraft) {
        return service.createAccount(overdraft).id
    }

    class TestThread implements Runnable {

        def start = new Date()

        public TestThread() {
        }

        void run() {
            println "thread started: ${this}"

            (1..3).each { runTestSingle() }

            def end = new Date();

            long diff = end.getTime() - start.getTime();
            println "thread finished in ${diff} millis: ${this}"

        }

        private void runTestSingle() {
            def consumerAccounts = []
            (1..1000).each {
                int consumerAccountId = createAccount(10)
                consumerAccounts.push(consumerAccountId)
            }


            def accountList = [];

            for (int i = 0; i < 12; i++) {
                accountList.addAll(consumerAccounts)
            }
            Collections.shuffle(accountList)

            accountList.each { int consumerAccountId ->
                def req = new CreateTransfer(consumerAccountId, merchantAccountId, 1)
                service.transfer(req)
            }
        }
    }


}
