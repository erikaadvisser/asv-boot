package org.n_is_1.ximedes.asv_boot.service

import groovy.transform.CompileStatic
import org.n_is_1.ximedes.asv_boot.model.Account
import org.n_is_1.ximedes.asv_boot.model.Transfer
import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer
import org.springframework.stereotype.Service

/**
 * The main service class for all business actions
 */
@Service
@CompileStatic
class MainService {
    static final int CREATION_DELAY_MILLIS = 1_000 * 0

    int accountIdCounter = 0
    int transferIdCounter = 0

    HashMap<Integer, Account> accounts
    HashMap<Integer, Transfer> transfers

    DataOutputStream stream

    public MainService() {
        streamLoad()

        def file = new File('stream.ser')
        stream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file, true)))

        Thread.start {
            while (true) {
                try {
                    sleep(5 * 1000)
                    flush()
                }
                catch (Exception e) {
                    println "Flush thread notes: ${e}"
                }
            }
        }
    }

    public synchronized flush() {
        stream.flush()
    }

    public synchronized Account createAccount(int overdraft) {
        accountIdCounter += 1

        def account = new Account(
                id: accountIdCounter,
                overdraft: overdraft,
//                creationMillis: System.currentTimeMillis()
        )

        accounts.put(account.id, account)

        saveAccount(account)

        return account
    }

    def synchronized transfer(CreateTransfer request) {
        if (accounts[request.from] == null || accounts[request.to] == null) {
            return createTransfer(request, Transfer.Status.ACCOUNT_NOT_FOUND)
        }

        Account from = accounts[request.from]
        if (from.balance + from.overdraft < request.amount) {
            return createTransfer(request, Transfer.Status.INSUFFICIENT_FUNDS)
        }

        Account to = accounts[request.to]
        from.balance -= request.amount
        to.balance += request.amount

        return createTransfer(request, Transfer.Status.CONFIRMED)
    }

    private def createTransfer(CreateTransfer request, Transfer.Status status) {
        transferIdCounter += 1

        def transfer = new Transfer(
                id: transferIdCounter,
                from: request.from,
                to: request.to,
                amount: request.amount,
                status: status,
//                creationMillis: System.currentTimeMillis()
        )
        transfers[transfer.id] = transfer
        saveTransfer(transfer)

        return transfer
    }

    public synchronized Account getAccount(int id) {
        return accounts[id]
    }

    public synchronized Transfer getTransfer(int id) {
        return transfers[id]
    }

    def createAccounts() {
//        300301
        Date start = new Date()

        for (int i = 0; i < 300301; i++) {
            createAccount(0)
        }
        println "last account id: ${accountIdCounter}"
        Date end = new Date();

        long diff = end.getTime() - start.getTime();
        println "createAccounts took: ${diff} millis"

    }

    def createTransfers() {
//        3601000
        println "start createTransfers"
        Date start = new Date()
        def request = new CreateTransfer(1, 1, 1)
        for (int i = 0; i < 3601000; i++) {
            createTransfer(request, Transfer.Status.CONFIRMED)
        }

        Date end = new Date();
        long diff = end.getTime() - start.getTime();
        println "createTransfers took: ${diff} millis"
    }

    def saveAccount(Account account) {
        stream.writeUTF("a")
        stream.writeInt(account.id);
        stream.writeInt(account.overdraft);
        stream.writeInt(account.balance);
    }

    def saveTransfer(Transfer transfer) {
        stream.writeUTF("t")
        stream.writeInt(transfer.id);
        stream.writeInt(transfer.from);
        stream.writeInt(transfer.to);
        stream.writeInt(transfer.amount);
        stream.writeByte(transfer.status.ordinal());
    }

    Account loadAccount(DataInputStream dataIn) {
        int id, overdraft, balance
        id = dataIn.readInt()
        overdraft = dataIn.readInt()
        balance = dataIn.readInt()
        return new Account(id: id,
                overdraft: overdraft,
                balance: balance)
    }

    Transfer loadTransfer(DataInputStream dataIn) {
        int id, from, to, amount, statusOrdinal

        id = dataIn.readInt()
        from = dataIn.readInt()
        to = dataIn.readInt()
        amount = dataIn.readInt()
        statusOrdinal = dataIn.readByte()

        return new Transfer(id: id,
                from: from,
                to: to,
                amount: amount,
                status: Transfer.Status.values()[statusOrdinal])
    }

    def synchronized streamLoad() {

        Date start = new Date()
        println "start streamload"

        clear()

        def file = new File('stream.ser')
        if (!file.exists()) {
            println "No old state to read, starting FRESH."
            return
        }

        def dataIn = new DataInputStream(new BufferedInputStream(new FileInputStream(file)))

        String indicator
        while (dataIn.available() > 0) {
            indicator = dataIn.readUTF()

            if (indicator == "a") {
                def account = loadAccount(dataIn)
                accounts[account.id] = account
                if (account.id > this.accountIdCounter) {
                    this.accountIdCounter = account.id
                }

            } else if (indicator == "t") {
                def transfer = loadTransfer(dataIn)
                transfers[transfer.id] = transfer
                if (transfer.id > this.transferIdCounter) {
                    this.transferIdCounter = transfer.id
                }

                if (transfer.status == Transfer.Status.CONFIRMED) {
                    Account from = accounts[transfer.from]
                    Account to = accounts[transfer.to]
                    from.balance -= transfer.amount
                    to.balance += transfer.amount
                }
            } else {
                println "unknonw entry in stream ${indicator}"
                throw new RuntimeException()
            }
        }

        dataIn.close()

        Date end = new Date();
        long diff = end.getTime() - start.getTime();
        println "Stream took: ${diff} millis"
        println ""
        printBalance()
        println ""
    }


    def printBalance() {
        long total = 0;
        accounts.each { int id, Account account ->
            total += account.balance
        }
        println ""
        println "Accounts: ${accounts.size()}"
        println "Balance: ${total}"

        int successTransfers = 0;
        int failTransfers = 0;
        int otherTransfers = 0;

        transfers.each { int id, Transfer transfer ->
            if (transfer.status == Transfer.Status.CONFIRMED) {
                successTransfers += 1
            } else if (transfer.status == Transfer.Status.INSUFFICIENT_FUNDS) {
                failTransfers += 1
            } else {
                otherTransfers += 1
            }
        }
        println "Transfers: ${transfers.size()}"
        println "Success: ${successTransfers}"
        println "Failure: ${failTransfers}"
        println "Other: ${otherTransfers}"

        Set set = new HashSet()
        transfers.each { int id, Transfer transfer ->
            set.add(transfer)
        }
        println "Unique transfers ${set.size()}"

    }

    def clear() {
        this.accountIdCounter = 0
        this.transferIdCounter = 0

        this.accounts = new HashMap<Integer, Account>()
        this.transfers = new HashMap<Integer, Transfer>()
    }

    def synchronized clean() {
        this.stream.close()
        def file = new File('stream.ser')
        if (file.exists()) {
            file.delete()
        }
        clear()
        stream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file, false)))

    }
}
