package org.n_is_1.ximedes.asv_boot.web

import org.n_is_1.ximedes.asv_boot.model.Account
import org.n_is_1.ximedes.asv_boot.model.Transfer
import org.n_is_1.ximedes.asv_boot.service.MainService
import org.n_is_1.ximedes.asv_boot.service.StressTester
import org.n_is_1.ximedes.asv_boot.web.model.CreateAccount
import org.n_is_1.ximedes.asv_boot.web.model.CreateTransfer
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 * Controller for all ASV calls.
 */
@RestController
public class MainController {

    static final def logback = LoggerFactory.getLogger(MainController)


    @Autowired MainService mainService

    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public ResponseEntity createAccount(@RequestBody CreateAccount request) {
        def account = mainService.createAccount(request.overdraft)

        HttpHeaders headers = new HttpHeaders()
        headers.setLocation(new URI("/account/${account.id}"))
        return new ResponseEntity<Void>(headers, HttpStatus.ACCEPTED)
    }

    @RequestMapping(value = '/account/{accountId}', method = RequestMethod.GET)
    public def getAccount(@PathVariable(value = "accountId") int accountId) {
        Account account = mainService.getAccount(accountId)

        if (!account) {
            return ResponseEntity.notFound().build()
        }

        return [accountId: Integer.toString(account.id),
                balance  : account.balance,
                overdraft: account.overdraft ]

    }

    @RequestMapping(value = "/transfer", method = RequestMethod.POST)
    public ResponseEntity createTransfer(@RequestBody CreateTransfer request) {
        def transfer = mainService.transfer(request)
        HttpHeaders headers = new HttpHeaders()
        headers.setLocation(new URI("/transfer/${transfer.id}"))
        return new ResponseEntity<Void>(headers, HttpStatus.ACCEPTED)
    }

    @RequestMapping(value = '/transfer/{transferId}', method = RequestMethod.GET)
    public def getTransfer(@PathVariable(value = "transferId") int transferId) {
        Transfer transfer = mainService.getTransfer(transferId)

        if (!transfer) {
            return ResponseEntity.notFound().build()
        }

        return [transactionId: Integer.toString(transfer.id),
                from         : Integer.toString(transfer.from),
                to           : Integer.toString(transfer.to),
                amount       : transfer.amount,
                status       : transfer.status ]
    }

    @RequestMapping(value = '/transaction/{transactionId}', method = RequestMethod.GET)
    public def getTransaction(@PathVariable(value = "transactionId") int transactionId) {
        Transfer transfer = mainService.getTransfer(transactionId)

        if (!transfer || transfer.status != Transfer.Status.CONFIRMED) {
            return ResponseEntity.notFound().build()
        }

        return [transactionId: Integer.toString(transfer.id),
                from         : Integer.toString(transfer.from),
                to           : Integer.toString(transfer.to),
                amount       : transfer.amount ]
    }

    @RequestMapping(value = "/health", method = RequestMethod.GET)
    public ResponseEntity health() {
        return ResponseEntity.ok().build()
    }

    @RequestMapping(value = "/supersave", method = RequestMethod.GET)
    public String supersave() {
        mainService.supersave()
        return "supersave"
    }

    @RequestMapping(value = "/superload", method = RequestMethod.GET)
    public String superload() {
        mainService.superload()
        return "superload"
    }

    @RequestMapping(value = "/createAccounts", method = RequestMethod.GET)
    public String createAccounts() {
        mainService.createAccounts()
        return "done"
    }

    @RequestMapping(value = "/createTransfers", method = RequestMethod.GET)
    public String createTransfers() {
        println "enter createTransfers"

        mainService.createTransfers()
        return "done"
    }

    @RequestMapping(value = "/clear", method = RequestMethod.GET)
    public String clear() {
        println "enter clear"

        mainService.clear()
        mainService.printBalance()
        return "done"
    }

    @RequestMapping(value = "/clean", method = RequestMethod.GET)
    public String clean() {
        println "enter clean"

        mainService.clean()
        mainService.printBalance()
        return "done"
    }

    @RequestMapping(value = "/stressTest", method = RequestMethod.GET)
    public String stressTest() {
        println "start stressTest"

        def tester = new StressTester(mainService)
        tester.runTest()

        return "end stressTest"
    }

    @RequestMapping(value = "/balance", method = RequestMethod.GET)
    public String printBalance() {
        println "start balance"

        try {
            mainService.printBalance()
        }
        catch (Exception exception) {
            logback.error("problem printing balance", exception)
        }


        return "end balance"
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Void> defaultErrorHandler(Exception e) throws Exception {
        logback.error("Caught exception thrown by controller", e)
        return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR)
    }

}