package org.n_is_1.ximedes.asv_boot.web.model

/**
 * A REST request to perofrm a transfer
 */
class CreateTransfer {
    int from
    int to
    int amount

    CreateTransfer() {
    }

    CreateTransfer(int from, int to, int amount) {
        this.from = from
        this.to = to
        this.amount = amount
    }
}
