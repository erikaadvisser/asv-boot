CREATE TABLE account (
  id             INTEGER PRIMARY KEY,
  overdraft      INTEGER NOT NULL,
  balance        INTEGER NOT NULL,
  creationMillis BIGINT  NOT NULL
);

CREATE TABLE transfer (
  id             INTEGER PRIMARY KEY,
  from_id        INTEGER NOT NULL,
  to_id          INTEGER NOT NULL,
  amount         INTEGER NOT NULL,
  creationMillis BIGINT  NOT NULL,
  status         TEXT    NOT NULL
);
